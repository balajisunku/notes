package com.balajisunku.notes.debug;

public class DLogger {

    private static final String TAG = "NotesApplicationLogs";

    public static void d(String tag, String message){
        android.util.Log.d(TAG, tag + " - " + message);
    }

    public static void e(String tag, String message){
        android.util.Log.e(TAG, tag + " - " + message);
    }

    public static void i(String tag, String message){
        android.util.Log.i(TAG, tag + " - " + message);
    }

    public static void v(String tag, String message){
        android.util.Log.v(TAG, tag + " - " + message);
    }

}
