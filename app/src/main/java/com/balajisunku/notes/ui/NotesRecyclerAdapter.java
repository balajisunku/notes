package com.balajisunku.notes.ui;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.balajisunku.notes.R;
import com.balajisunku.notes.database.Note;
import com.balajisunku.notes.debug.DLogger;

import java.lang.ref.WeakReference;
import java.util.List;

public class NotesRecyclerAdapter extends RecyclerView.Adapter<NotesRecyclerAdapter.RecyclerViewHolder> {

    private static final String TAG = "NotesRecyclerAdapter";

    private List<Note> notesList;
    private WeakReference<NoteClickedListener> onClickListener;

    public NotesRecyclerAdapter(List<Note> notesList, NoteClickedListener onClickListener) {
        this.notesList = notesList;
        this.onClickListener = new WeakReference<>(onClickListener);
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        DLogger.d(TAG, "onCreateViewHolder: ");
        return new RecyclerViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.note_item_card, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, int position) {
        DLogger.d(TAG, "onBindViewHolder position : "+position);
        final Note note = notesList.get(position);
        holder.titleTextView.setText(note.getTitle());
        holder.descriptionTextView.setText(note.getDescription());
        holder.dateTextView.setText(note.getLastModified().toLocaleString().substring(0, 11));
        holder.itemView.setTag(note);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(onClickListener != null && onClickListener.get() != null){
                    onClickListener.get().onNoteClicked(note);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return notesList.size();
    }

    public void addItems(List<Note> notesList) {
        this.notesList = notesList;
        notifyDataSetChanged();
    }

    static class RecyclerViewHolder extends RecyclerView.ViewHolder {
        private TextView titleTextView;
        private TextView descriptionTextView;
        private TextView dateTextView;

        RecyclerViewHolder(View view) {
            super(view);
            titleTextView = view.findViewById(R.id.titleTextView);
            descriptionTextView = view.findViewById(R.id.descriptionTextView);
            dateTextView = view.findViewById(R.id.dateTextView);
        }
    }

    public interface NoteClickedListener{
        public void onNoteClicked(Note note);
    }
}

