package com.balajisunku.notes.ui;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.balajisunku.notes.NotesApplication;
import com.balajisunku.notes.R;
import com.balajisunku.notes.database.Note;
import com.balajisunku.notes.di.NoteViewModel;

import java.util.Date;

import javax.inject.Inject;

public class AddNoteActivity extends AppCompatActivity {

    private static final String TAG = "AddNoteActivity";

    private EditText titleEditText;
    private EditText descriptionEditText;

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    NoteViewModel noteViewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_note_activity_layout);

        ((NotesApplication)getApplication()).getApplicationComponent().inject(this);

        titleEditText = findViewById(R.id.titleEditText);
        descriptionEditText = findViewById(R.id.descriptionEditText);

        noteViewModel = ViewModelProviders.of(this, viewModelFactory).get(NoteViewModel.class);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_add_note, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_done:
                if(isInputValid()){
                    Note note = new Note(titleEditText.getText().toString(), descriptionEditText.getText().toString(), new Date(), new Date());
                    noteViewModel.addNewNote(note);
                    finish();
                }else{
                    Toast.makeText(this, getString(R.string.input_error), Toast.LENGTH_SHORT).show();
                }
                break;
        }
        return false;
    }

    private boolean isInputValid(){

        if(titleEditText.getText().toString().isEmpty() || descriptionEditText.getText().toString().isEmpty()){
            return false;
        }
        return true;

    }
}
