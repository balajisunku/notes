package com.balajisunku.notes.ui;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

import com.balajisunku.notes.NotesApplication;
import com.balajisunku.notes.R;
import com.balajisunku.notes.database.Note;
import com.balajisunku.notes.di.NotesCollectionViewModel;

import javax.inject.Inject;

public class NoteDetailActivity extends AppCompatActivity {

    private static final String TAG = "NoteDetailActivity";

    public static final String EXTRA_NOTE_ID = "note_id";
    public static final String EXTRA_TITLE_TEXT = "title_text";
    public static final String EXTRA_DESCRIPTION_TEXT = "description_text";
    public static final String EXTRA_DATE_TEXT = "date_text";

    private NotesCollectionViewModel notesViewModel;

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    private TextView titleTextView;
    private TextView descriptionTextView;
    private TextView dateTextView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_note_detail_activity);

        ((NotesApplication)getApplication()).getApplicationComponent().inject(this);
        notesViewModel = ViewModelProviders.of(this, viewModelFactory).get(NotesCollectionViewModel.class);

        titleTextView = findViewById(R.id.titleTextView);
        descriptionTextView = findViewById(R.id.descriptionTextView);
        dateTextView = findViewById(R.id.dateTextView);

        titleTextView.setText(getIntent().getStringExtra(EXTRA_TITLE_TEXT));
        descriptionTextView.setText(getIntent().getStringExtra(EXTRA_DESCRIPTION_TEXT));
        dateTextView.setText(getIntent().getStringExtra(EXTRA_DATE_TEXT));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_detail_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_delete:
                notesViewModel.deleteNote(new Note(getIntent().getIntExtra(EXTRA_NOTE_ID, 0)));
                finish();
                break;
        }
        return false;
    }
}
