package com.balajisunku.notes.database;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.TypeConverters;

import java.util.List;

@Dao
@TypeConverters(DateConverter.class)
public interface NotesDao {

    @Query("select * from Note")
    LiveData<List<Note>> getAllNotes();

    @Query("select * from Note where id = :id")
    Note getNoteById(String id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void addNote(Note note);

    @Delete
    void deleteNote(Note note);

}
