package com.balajisunku.notes.database;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;

import java.util.Date;

@Entity
public class Note {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private String title;
    private String description;
    @TypeConverters(DateConverter.class)
    private Date lastModified;
    @TypeConverters(DateConverter.class)
    private Date created;

    @Ignore
    public Note(int id){
        this.id = id;
    }

    public Note(String title, String description, Date lastModified, Date created) {
        this.title = title;
        this.description = description;
        this.lastModified = lastModified;
        this.created = created;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public Date getCreated() {
        return created;
    }
}
