package com.balajisunku.notes.database;

import android.arch.lifecycle.LiveData;

import java.util.List;

import javax.inject.Inject;

public class NotesRepository {

    private final NotesDao notesDao;

    @Inject
    public NotesRepository(NotesDao notesDao) {
        this.notesDao = notesDao;
    }

    public LiveData<List<Note>> getAllNotes(){
        return notesDao.getAllNotes();
    }

    public void insertNote(Note note){
        notesDao.addNote(note);
    }

    public void deleteNote(Note note){
        notesDao.deleteNote(note);
    }

}
