package com.balajisunku.notes.di;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.balajisunku.notes.database.NotesRepository;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by balaji.sunku on 30/01/18.
 */

@Singleton
public class CustomViewModelFactory implements ViewModelProvider.Factory {

    private final NotesRepository repository;

    @Inject
    public CustomViewModelFactory(NotesRepository repository) {
        this.repository = repository;
    }

    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {

        if(modelClass.isAssignableFrom(NoteViewModel.class)){
            return (T) new NoteViewModel(repository);
        }

        else if (modelClass.isAssignableFrom(NotesCollectionViewModel.class)){
            return (T) new NotesCollectionViewModel(repository);
        }

        else {
            throw new IllegalArgumentException("ViewModel not found");
        }

    }
}
