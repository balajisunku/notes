package com.balajisunku.notes.di;

import android.arch.lifecycle.ViewModel;
import android.os.AsyncTask;

import com.balajisunku.notes.database.Note;
import com.balajisunku.notes.database.NotesRepository;

public class NoteViewModel extends ViewModel {

    private NotesRepository repository;

    public NoteViewModel(NotesRepository repository){
        this.repository = repository;
    }

    public void addNewNote(Note note){
        new AddItemTask().execute(note);
    }

    private class AddItemTask extends AsyncTask<Note, Void, Void> {

        @Override
        protected Void doInBackground(Note... item) {
            repository.insertNote(item[0]);
            return null;
        }
    }

}
