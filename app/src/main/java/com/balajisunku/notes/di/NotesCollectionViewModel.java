package com.balajisunku.notes.di;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.os.AsyncTask;

import com.balajisunku.notes.database.Note;
import com.balajisunku.notes.database.NotesRepository;

import java.util.List;

/**
 * Created by balaji.sunku on 30/01/18.
 */

public class NotesCollectionViewModel extends ViewModel {

    private NotesRepository repository;

    public NotesCollectionViewModel(NotesRepository repository){
        this.repository = repository;
    }

    public LiveData<List<Note>> getAllNotes() {
        return repository.getAllNotes();
    }

    public void deleteNote(Note note) {
        DeleteNoteTask deleteItemTask = new DeleteNoteTask();
        deleteItemTask.execute(note);
    }

    public void addNote(Note note){
        InsertNoteTask insertNoteTask = new InsertNoteTask();
        insertNoteTask.execute(note);
    }

    private class InsertNoteTask extends AsyncTask<Note, Void, Void> {

        @Override
        protected Void doInBackground(Note... item) {
            repository.insertNote(item[0]);
            return null;
        }
    }

    private class DeleteNoteTask extends AsyncTask<Note, Void, Void> {

        @Override
        protected Void doInBackground(Note... item) {
            repository.deleteNote(item[0]);
            return null;
        }
    }

}
