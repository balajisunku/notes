package com.balajisunku.notes.di;

import com.balajisunku.notes.MainActivity;
import com.balajisunku.notes.ui.AddNoteActivity;
import com.balajisunku.notes.ui.NoteDetailActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class, RoomModule.class})
public interface ApplicationComponent {

    void inject(AddNoteActivity addNoteActivity);
    void inject(MainActivity mainActivity);
    void inject(NoteDetailActivity noteDetailActivity);

}
