package com.balajisunku.notes.di;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.persistence.room.Room;

import com.balajisunku.notes.Constants;
import com.balajisunku.notes.database.NotesDao;
import com.balajisunku.notes.database.NotesDatabase;
import com.balajisunku.notes.database.NotesRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class RoomModule {

    private final NotesDatabase database;

    public RoomModule(Application application) {
        this.database = Room.databaseBuilder(
                application,
                NotesDatabase.class,
                Constants.DATABASE_NAME
        ).build();
    }

    @Provides
    @Singleton
    NotesRepository provideNotesRepository(NotesDao notesDao){
        return new NotesRepository(notesDao);
    }

    @Provides
    @Singleton
    NotesDao provideNotesDao(NotesDatabase database){
        return database.notesDao();
    }

    @Provides
    @Singleton
    NotesDatabase provideNotesDatabase(Application application){
        return database;
    }

    @Provides
    @Singleton
    ViewModelProvider.Factory provideViewModelFactory(NotesRepository repository){
        return new CustomViewModelFactory(repository);
    }

}
