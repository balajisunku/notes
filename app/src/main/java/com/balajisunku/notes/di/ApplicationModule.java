package com.balajisunku.notes.di;

import android.app.Application;

import com.balajisunku.notes.NotesApplication;

import dagger.Module;
import dagger.Provides;

/**
 * Created by balaji.sunku on 30/01/18.
 */

@Module
public class ApplicationModule {

    private final NotesApplication notesApplication;

    public ApplicationModule(NotesApplication notesApplication) {
        this.notesApplication = notesApplication;
    }

    @Provides
    NotesApplication provideNotesApplication(){
        return notesApplication;
    }

    @Provides
    Application provideApplication(){ return notesApplication; }
}
