package com.balajisunku.notes;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.balajisunku.notes.database.Note;
import com.balajisunku.notes.debug.DLogger;
import com.balajisunku.notes.di.NotesCollectionViewModel;
import com.balajisunku.notes.ui.AddNoteActivity;
import com.balajisunku.notes.ui.NoteDetailActivity;
import com.balajisunku.notes.ui.NotesRecyclerAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity implements NotesRecyclerAdapter.NoteClickedListener {

    private static final String TAG = "MainActivity";
    
    private NotesCollectionViewModel notesViewModel;
    private NotesRecyclerAdapter notesAdapter;
    private RecyclerView notesRecyclerView;

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ((NotesApplication)getApplication()).getApplicationComponent().inject(this);

        notesRecyclerView = findViewById(R.id.notesRecyclerView);
        notesAdapter = new NotesRecyclerAdapter(new ArrayList<Note>(), this);
        notesRecyclerView.setLayoutManager(new GridLayoutManager(this, 2));

        notesRecyclerView.setAdapter(notesAdapter);

        notesViewModel = ViewModelProviders.of(this, viewModelFactory).get(NotesCollectionViewModel.class);

        notesViewModel.getAllNotes().observe(MainActivity.this, new Observer<List<Note>>() {
            @Override
            public void onChanged(@Nullable List<Note> notesList) {
                DLogger.d(TAG, "onChanged: ");
                notesAdapter.addItems(notesList);
            }
        });

    }

    @Override
    public void onNoteClicked(Note note) {
        DLogger.d(TAG, "onNoteClicked: ");
        Intent intent = new Intent(this, NoteDetailActivity.class);
        intent.putExtra(NoteDetailActivity.EXTRA_NOTE_ID, note.getId());  // Note can be converted into Parcelable and sent in intent extra
        intent.putExtra(NoteDetailActivity.EXTRA_TITLE_TEXT, note.getTitle());
        intent.putExtra(NoteDetailActivity.EXTRA_DESCRIPTION_TEXT, note.getDescription());
        intent.putExtra(NoteDetailActivity.EXTRA_DATE_TEXT, Util.getFormattedDate(note.getCreated()));
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_done:
                Intent intent = new Intent(this, AddNoteActivity.class);
                startActivity(intent);
                break;
        }
        return false;
    }
}
