package com.balajisunku.notes;

import android.app.Application;

import com.balajisunku.notes.di.ApplicationComponent;
import com.balajisunku.notes.di.ApplicationModule;
import com.balajisunku.notes.di.DaggerApplicationComponent;
import com.balajisunku.notes.di.RoomModule;
import com.facebook.stetho.Stetho;

public class NotesApplication extends Application {

    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        Stetho.initializeWithDefaults(this);

        applicationComponent = DaggerApplicationComponent
                .builder()
                .applicationModule(new ApplicationModule(this))
                .roomModule(new RoomModule(this))
                .build();
    }

    public ApplicationComponent getApplicationComponent(){
        return applicationComponent;
    }
}
