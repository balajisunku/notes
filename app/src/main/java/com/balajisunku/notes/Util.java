package com.balajisunku.notes;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Util {

    public static String getFormattedDate(Date date){
        SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
        return formatter.format(date);
    }

}
